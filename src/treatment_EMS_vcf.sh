#!/bin/bash

# filter the mutations identified in each EMS
python filter_vcf.py --infiles GY2402_S1.vcf GY2403_S2.vcf GY2404_S3.vcf GY2405_S4.vcf GY2406_S5.vcf GY2407_S6.vcf GY2408_S7.vcf GY2409_S8.vcf GY2410_S9.vcf --outfiles filter_GY2402_S1.vcf filter_GY2403_S2.vcf filter_GY2404_S3.vcf filter_GY2405_S4.vcf filter_GY2406_S5.vcf filter_GY2407_S6.vcf filter_GY2408_S7.vcf filter_GY2409_S8.vcf filter_GY2410_S9.vcf

# count occurance of each mutation and keep only unique
python filter_occurances.py --infiles filter_GY2402_S1.vcf filter_GY2403_S2.vcf filter_GY2404_S3.vcf filter_GY2405_S4.vcf filter_GY2406_S5.vcf filter_GY2407_S6.vcf filter_GY2408_S7.vcf filter_GY2409_S8.vcf filter_GY2410_S9.vcf --outfiles final_vcf_1.vcf final_vcf_2.vcf final_vcf_3.vcf final_vcf_4.vcf final_vcf_5.vcf final_vcf_6.vcf final_vcf_7.vcf final_vcf_8.vcf final_vcf_9.vcf

