"""
To run this script, open a terminal and run the following command : 
python filter_vcf.py --infiles <file1>.vcf <file2>.vcf --outfiles filter_<file1>.vcf filter_<file2>.vcf
"""

import vcf
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
	"--infiles",
	nargs="*",
	type=str,
	default=[],
)
parser.add_argument(
	"--outfiles",
	nargs="*",
	type=str,
	default=[],
)

args = parser.parse_args()

input_data = args.infiles
output_data = args.outfiles

for i in range(len(input_data)):

	vcf_reader = vcf.Reader(open(args.infiles[i], 'r'))

	vcf_writer = vcf.Writer(open(args.outfiles[i], 'w'), vcf_reader)


	variants_after_filter = 0
	variants_before_filter = 0

	# EMS mutants have only one bulk so we can filter using INFO
	for record in vcf_reader:
		variants_before_filter += 1
		# BEGIN FILTERING
		if (record.is_snp):
			# on filtre sur la qualité
			if (record.QUAL > 200.0):
				# on filtre sur la profondeur DP
				if (record.INFO['DP'] > 5):
					if (record.INFO['AO'][0] > 30):
						if (record.INFO['MQM'][0] > 27):
							if (record.INFO['PAIRED'][0] > 0.8):
								if (record.INFO['EPP'][0] < 50):
									if (record.INFO['RPP'][0] < 50):
										if (record.INFO['SAP'][0] < 100):

											freqRO = record.INFO['RO'] / (record.INFO['AO'][0] + record.INFO['RO'])
											
											if (freqRO < 0.2):
												# END FILTERING
												# the resulting variants are written in vcf_writer
												vcf_writer.write_record(record)

												variants_after_filter += 1
	print("file : ", args.outfiles[i])
	print("total number of variants before filters : ", variants_before_filter)
	print("total number of variants after filters : ", variants_after_filter)

