"""
To run this script, open a terminal and run the following command : 
python compare_EMS_BSA.py <EMS_INPUT_FILE>.vcf <BSA_INPUT_FILE>.vcf <common_mutations_EMS_BSA>.txt <BSA_freq_alleles>.txt

Example : 
python compare_EMS_BSA.py final_vcf_5.vcf 2P17F03.vcf common_mutations_EMS_BSA_2P17F03.txt BSA_2P17F03_freq_alleles.txt
"""

import vcf
import sys

### FUNCTIONS 

def matrix(input_file):
	"""
	Read input_file and create a matrix with useful informations
	"""
	CHROM = []
	POS = []
	REF = []
	ALT = []
	
	for record in input_file :
		# print(record)
		CHROM.append(record.CHROM)
		#print(record.CHROM)
		POS.append(record.POS)
		#print(record.POS)
		REF.append(record.REF)
		#print(record.REF)
		ALT.append(record.ALT[0])
		#print(record.ALT[0])
	MATRIX = [CHROM, POS, REF, ALT]
	return MATRIX
	
	
def compare(matrix1, matrix2, BSA_file):
	"""
	Compare the informations between the 2 matrices
	The line appearing in both are written in tmp_file
	Display lines that don't appear in both matrices
	"""
	chrom = ""
	pos = ""
	ref = ""
	alt = ""
	for i in range(len(matrix1[0])):
		printed = False
		for j in range(len(matrix2[0])):
			if (matrix1[0][i] == matrix2[0][j] and matrix1[1][i] == matrix2[1][j] and matrix1[2][i] == matrix2[2][j] and matrix1[3][i] == matrix2[3][j]):
				printed = True
				chrom = str(matrix1[0][i])
				pos = str(matrix1[1][i]) 
				ref = str(matrix1[2][i]) 
				alt = str(matrix1[3][i]) 
				line_to_find = chrom+'\t'+pos+'\t'+"."+'\t'+ref+'\t'+alt+'\t'
				# tmp_file.write(line_to_find+'\n')
				
				tmp_file.writelines([line for line in open(BSA_file) if line_to_find in line])
		
		if (printed == False):
			print("\n           /!\ WARNING ")
			print("This mutation appears in EMS but not in BSA")
			chrom = str(matrix1[0][i])
			pos = str(matrix1[1][i])
			ref = str(matrix1[2][i])
			alt = str(matrix1[3][i])
			print(chrom + " " + pos + " " + ref + " " + alt)


### MAIN PROGRAM

vcf_reader_EMS = vcf.Reader(open(sys.argv[1], 'r'))
vcf_reader_BSA = vcf.Reader(open(sys.argv[2], 'r'))
BSA_file = sys.argv[2]
tmp_file = open(sys.argv[3],'w')
output_file = open(sys.argv[4],'w')

EMS_MATRIX = matrix(vcf_reader_EMS)
"""
print(EMS_MATRIX[0][0])
print(EMS_MATRIX[1][0])
print(EMS_MATRIX[2][0])
print(EMS_MATRIX[3][0])
print(len(EMS_MATRIX[0]))
"""


BSA_MATRIX = matrix(vcf_reader_BSA)
"""
print(BSA_MATRIX[0][0])
print(BSA_MATRIX[1][0])
print(BSA_MATRIX[2][0])
print(BSA_MATRIX[3][0])

print("number of variants in BSA.vcf : ", len(BSA_MATRIX[0]))
"""


# header_line is useful because we want to know the bulk's order		
header_line = '#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT'
# we extract the header line from the BSA_file and write it in tmp_file	
tmp_file.writelines([line for line in open(BSA_file) if header_line in line])

# use compare to write in tmp_file the mutations (lines) appearing in both files (EMS.vcf and BSA.vcf)
compare(EMS_MATRIX, BSA_MATRIX, BSA_file)

tmp_file.close()

# Now we want to collect RO and AO data for each bulk and for each mutation (common to EMS and BSA)
tmp_file = open(sys.argv[3],'r')

RO_1 = []
RO_2 = []
RO_3 = []
AO_1 = []
AO_2 = []
AO_3 = []

low = 0
middle = 0
high = 0

for line in tmp_file:
	if header_line in line :
		pos = 0
		for word in line.split():
			pos += 1
			if (word == 'L') : # to keep in memory the bulk's order
				low = pos
			elif (word == 'M' ):
				middle = pos	
			elif (word == 'H') :	
				high = pos	
	else : 
		list_of_words = []
		for word in line.split(): # each line is split in words which are stored in a list 
			list_of_words.append(word)
			
		RO_1.append(list_of_words[-3].split(':')[3]) # -3 : first bulk, 3rd element of the list is RO and 5th element of the list is AO
		AO_1.append(list_of_words[-3].split(':')[5])
		RO_2.append(list_of_words[-2].split(':')[3]) # -2 : second bulk
		AO_2.append(list_of_words[-2].split(':')[5])
		RO_3.append(list_of_words[-1].split(':')[3]) # -1 : last bulk
		AO_3.append(list_of_words[-1].split(':')[5])

# for each bulk, we associate the corresponding RO and AO data
# the bulk's order is important for this step !
if (low < middle < high):
	low_bulk = (RO_1, AO_1)
	middle_bulk = (RO_2, AO_2)
	high_bulk = (RO_3, AO_3)
elif (middle < high < low):
	low_bulk = (RO_3, AO_3)
	middle_bulk = (RO_1, AO_1)
	high_bulk = (RO_2, AO_2)
elif (high < low < middle):
	low_bulk = (RO_2, AO_2)
	middle_bulk = (RO_3, AO_3)
	high_bulk = (RO_1, AO_1)
elif (low < high < middle):
	low_bulk = (RO_1, AO_1)
	middle_bulk = (RO_3, AO_3)
	high_bulk = (RO_2, AO_2)
elif (middle < low < high):
	low_bulk = (RO_2, AO_2)
	middle_bulk = (RO_1, AO_1)
	high_bulk = (RO_3, AO_3)
elif (high < middle < low):
	low_bulk = (RO_3, AO_3)
	middle_bulk = (RO_2, AO_2)
	high_bulk = (RO_1, AO_1)

# Write our output file with his different rows
header_line_output = 'CHROM	POS	REF	ALT	RO:L	AO:L	RO:M	AO:M	RO:H	AO:H'
output_file.write(header_line_output+'\n')

tmp_file.close() # need to close and reopen it because it was already read

tmp_file = open(sys.argv[3],'r')

# collect informations about each mutation
chrom = []
pos = []
ref = []
alt = []
for line in tmp_file:
	list_of_words = []
	if header_line not in line :
		for word in line.split() :
			list_of_words.append(word)
			
		chrom.append(list_of_words[0])
		pos.append(list_of_words[1])
		ref.append(list_of_words[3])
		alt.append(list_of_words[4])

# Fill up the different rows in our output_file
for i in range(len(chrom)):
	output_file.write(chrom[i]+'\t'+pos[i]+'\t'+ref[i]+'\t'+alt[i]+'\t'+low_bulk[0][i]+'\t'+low_bulk[1][i]+'\t'+middle_bulk[0][i]+'\t'+middle_bulk[1][i]+'\t'+high_bulk[0][i]+'\t'+high_bulk[1][i]+'\n')


output_file.close()
tmp_file.close()

