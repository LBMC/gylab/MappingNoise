"""
To run this script, open a terminal and run the following command : 
python filter_vcf.py --infiles filter_<file1>.vcf filter_<file2>.vcf --outfiles final_<file1>.vcf final_<file2>.vcf
"""

import vcf
import argparse

### FUNCTIONS

def big_matrix(input_data):
	"""
	Read input_data and create a matrix containing all mutations from all files
	"""
	CHROM = []
	POS = []
	REF = []
	ALT = []
	OCC = []
	for i in range(len(input_data)):
		vcf_reader = vcf.Reader(open(input_data[i], 'r'))
		for record in vcf_reader:
			CHROM.append(record.CHROM)
			POS.append(record.POS)
			REF.append(record.REF)
			ALT.append(record.ALT[0])
			OCC.append(0)
	MATRIX = [CHROM, POS, REF, ALT, OCC]
	return MATRIX
	

def count_occ(matrice):
	"""
	count how many time a mutation appears in the matrix
	"""	
	for j in range(len(matrice[0])):
		occ = 0
		for k in range(len(matrice[0])):
			if (matrice[0][j]==matrice[0][k] and matrice[1][j]==matrice[1][k] and matrice[2][j]==matrice[2][k] and matrice[3][j]==matrice[3][k]):
				occ += 1
				matrice[0][k] == ""
				matrice[1][k] == ""
				matrice[2][k] == ""
				matrice[3][k] == ""
		matrice[4][j] = occ
	return matrice


def compare(matrice, vcf_in, vcf_out):
	vcf_reader = vcf.Reader(open(vcf_in, 'r'))
	vcf_output = vcf.Writer(open(vcf_out, 'w'), vcf_reader)
	
	for record in vcf_reader:
		
		for j in range(len(matrice[0])):
			
			if (record.CHROM == matrice[0][j] and record.POS == matrice[1][j] and record.REF == matrice[2][j] and record.ALT[0] == matrice[3][j] and matrice[4][j] == 1):
				vcf_output.write_record(record)
				


### MAIN PROGRAM 

parser = argparse.ArgumentParser()
parser.add_argument(
	"--infiles",
	nargs="*",
	type=str,
	default=[],
)
parser.add_argument(
	"--outfiles",
	nargs="*",
	type=str,
	default=[],
)

args = parser.parse_args()

input_data = args.infiles
output_data = args.outfiles

# create a matrix containing all mutations
BIG_MATRIX = big_matrix(input_data)

# count how many times a mutation appears (occ)
BIG_MATRIX_OCC = count_occ(BIG_MATRIX)

# texte file containing all mutations and their occurance in all files
tmp_file = open('occurance.txt','w')
header_line_output = 'CHROM	POS	REF	ALT	OCC'
tmp_file.write(header_line_output+'\n')

for i in range(len(BIG_MATRIX_OCC[0])):
	chrom = BIG_MATRIX_OCC[0][i]
	pos = str(BIG_MATRIX_OCC[1][i])
	ref = BIG_MATRIX_OCC[2][i]
	alt = str(BIG_MATRIX_OCC[3][i])
	occ = str(BIG_MATRIX_OCC[4][i])
		
	tmp_file.write(chrom+'\t'+pos+'\t'+ref+'\t'+alt+'\t'+occ+'\n')

tmp_file.close()


# each mutation in a vcf input is compared to the mutations in matrix 
# for each vcf input, output a vcf file containing all mutations occuring once
for i in range(len(input_data)):
	compare(BIG_MATRIX_OCC, input_data[i], output_data[i])


# Print some statistics
total_variants = 0	
mean = 0	
for vcf_data in output_data :
	vcf_reader = vcf.Reader(open(vcf_data, 'r'))
	countCtoT = 0
	countGtoA = 0
	variants = 0
	print("in file : ", vcf_data)
	for record in vcf_reader:
		variants += 1
		total_variants += 1
		if (record.REF == 'C'):
			if (record.ALT[0] == 'T'):
				countCtoT += 1
		elif (record.REF == 'G'):
			if (record.ALT[0] == 'A'):
				countGtoA += 1
	
	print("number of variants : ", variants)
	percent = (countCtoT + countGtoA)/variants * 100
	print("proportion (in %) of C -> T and G -> A mutations : ", percent)
	mean += percent
	
	
print("\nnumber of total variants : ", total_variants)
print("\nmean number of variants per EMS: ", total_variants/len(input_data))
print("\nmean proportion (in %) of C -> T and G -> A mutations : ", mean/len(input_data))

