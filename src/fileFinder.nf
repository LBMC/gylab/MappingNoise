params.input_csv = "data/test_input.csv"

Channel
  .fromPath(params.input_csv)
  .splitCsv(header: true, sep:"\t")
  .view()
  .into{files_infos; files_path}

Channel
  .fromFilePairs(
    files_path
      .map{it -> it.files}
      .collect()
      .getVal()
  )
  .join(
    files_infos
      .map{it -> [( it.files =~ /.*\/(.*)\{1,2\}.*/)[0][1], it.mutant, it.couverture_level]}
  )
  .map{it -> [it[0], it[2], it[3], [it[1]]]}
  .view()
