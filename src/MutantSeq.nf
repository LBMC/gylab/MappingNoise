/*
Mutant analysis pipeline

input:
- csv file containing mutant_id, coverage_level and fastq paths (to EMS or BSA data)
- fasta file (used as reference genome)

output:
- multiqc report (.html)
- vcf files
- insert_size.txt
- coverage.txt

Example for paired-end data for EMS dataset :
(PSMN run)
./nextflow src/MutantSeq.nf -c src/MutantSeq.config -profile psmn --fasta "data/tiny_dataset/ref_genome/*.fsa" --str 'EMS' --inputFromPATH "data/EMS_table_psmn.csv" -resume

Example for paired-end data for BSA dataset :
(PSMN run)
./nextflow src/MutantSeq.nf -c src/MutantSeq.config -profile psmn --fasta "data/tiny_dataset/ref_genome/*.fsa" --str 'BSA' --inputFromPATH "data/BSA_table_psmn.csv" -resume

*/

params.fasta = "data/ref_genome/*.fsa"
params.str = 'EMS'

log.info "fasta files : ${params.fasta}"

  if( params.str == 'EMS' )
    log.info "exp type files : ${params.str}"
  else if ( params.str == 'BSA' )
    log.info "exp type files : ${params.str}"
  else
    error "Invalid option --str"


Channel
  .fromPath( params.fasta )
  .ifEmpty { error "Cannot find any fasta files matching: ${params.fasta}" }
  .into { fasta_file; fasta_file2 }

/* Create a channel with csv file containing the mutant_id, the coverage_level and path to fastq files */
if (params.inputFromPATH)  {
      Channel
            .fromPath(params.inputFromPATH, checkIfExists: true)
            .set { fromPATH_csv }
}
else {
      Channel
            .empty()
            .ifEmpty { error "Cannot find any csv files matching: ${params.inputFromPATH}" }
      }

if (params.inputFromPATH){
            fromPATH_csv
                  .splitCsv(header:true, sep:"\t")
                  .into { files_infos ; files_path }
            
            Channel
              .fromFilePairs(
                files_path
                  .map{it -> it.files}
                  .collect()
                  .getVal()
              )
              .join(
                files_infos
                  .map{it -> [( it.files =~ /.*\/(.*)\{1,2\}.*/)[0][1], it.mutant_id, it.segregant_population]}
              )
              .map{it -> [it[2], it[3], [it[0], it[1]]]}
              .into { exp_fromPATH; exp_fromPATH1 }
 }
 else{
           Channel
                 .empty()
                 .set { exp_fromPATH }
 }

/* exp_fromPATH est de ce type :
[mutant_id, couverture_level, [pair_id, [/file1, /file2]]]
et data0 : 
[mutant_id, couverture_level, pair_id, [/file1, /file2]] */
  
exp_fromPATH
  .map { it ->  [ it[0],it[1],it[2][0],it[2][1] ]}
  .into { data0; data1 }


process fastqc_fastq_test {
  tag "$pair_id"
  label "fastqc"

  input:
  set mutant_id, couverture_level, pair_id, file(reads) from data0

  output:
    file "*.{zip,html}" into fastqc_report0

  script:
"""
fastqc --quiet --threads ${task.cpus} --format fastq --outdir ./ \
${reads[0]} ${reads[1]}
"""  
}

process multiqc_before_cut {
  tag "$report[0].baseName"
  publishDir "results/${params.str}/multiqc_before_cut/", mode: 'copy'
  label "multiqc"

  input:
    file report from fastqc_report0.collect()

  output:
    file "*multiqc_*" into multiqc_report0

  script:
"""
multiqc -f .
"""
}

process adaptor_removal {
  tag "$pair_id"
  label "cutadapt"

  input:
  set mutant_id, couverture_level, pair_id, file(reads) from data1

  output:
  set mutant_id, couverture_level, pair_id, "*_cut_R{1,2}.fastq.gz" into fastq_files1_cut, fastq_files2_cut

  script:
  if( params.str == 'EMS' )
    """
    cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCA \
    -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT \
    -o ${pair_id}_cut_R1.fastq.gz -p ${pair_id}_cut_R2.fastq.gz \
    ${reads[0]} ${reads[1]} > ${pair_id}_report.txt
    """
  else if ( params.str == 'BSA' )
    """
    cutadapt -g AATGATACGGCGACCACCGAGATCTACACNNNNNNNNTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG \
    -a CTGTCTCTTATACACATCTCCGAGCCCACGAGACNNNNNNNNATCTCGTATGCCGTCTTCTGCTTG \
    -G CAAGCAGAAGACGGCATACGAGATNNNNNNNNGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG \
    -A CTGTCTCTTATACACATCTGACGCTGCCGACGANNNNNNNNGTGTAGATCTCGGTGGTCGCCGTATCATT \
    -o ${pair_id}_cut_R1.fastq.gz -p ${pair_id}_cut_R2.fastq.gz \
    ${reads[0]} ${reads[1]} > ${pair_id}_report.txt
    """  
  else
    error "Invalid option --str"
}

process fastqc_fastq1 {
  tag "$pair_id"
  label "fastqc"

  input:
    set mutant_id, couverture_level, pair_id, file(reads) from fastq_files1_cut

  output:
    file "*.{zip,html}" into fastqc_report1

  script:
"""
fastqc --quiet --threads ${task.cpus} --format fastq --outdir ./ \
${reads[0]} ${reads[1]}
"""
}

process multiqc_after_cut {
  tag "$report[0].baseName"
  publishDir "results/${params.str}/multiqc_after_cut/", mode: 'copy'
  label "multiqc"

  input:
    file report from fastqc_report1.collect()

  output:
    file "*multiqc_*" into multiqc_report1

  script:
"""
multiqc -f .
"""
}

process trimming {
  tag "${reads}"
  label "trimming"

  input:
  set mutant_id, couverture_level, pair_id, file(reads) from fastq_files2_cut

  output:
  set mutant_id, couverture_level, pair_id, "*_trim_R{1,2}.fastq.gz" into fastq_files1_trim, fastq_files2_trim, fastq_files3_trim

  script:
"""
UrQt --t 20 --m ${task.cpus} --gz \
--in ${reads[0]} --inpair ${reads[1]} \
--out ${pair_id}_trim_R1.fastq.gz --outpair ${pair_id}_trim_R2.fastq.gz \
> ${pair_id}_trimming_report.txt
"""
}

process fastqc_fastq2 {
  tag "$pair_id"
  label "fastqc"

  input:
    set mutant_id, couverture_level, pair_id, file(reads) from fastq_files1_trim

  output:
    file "*.{zip,html}" into fastqc_report2

  script:
"""
fastqc --quiet --threads ${task.cpus} --format fastq --outdir ./ \
${reads[0]} ${reads[1]}
"""
}

process multiqc_after_trimm {
  tag "$report[0].baseName"
  publishDir "results/${params.str}/multiqc_after_trimm/", mode: 'copy'
  label "multiqc"

  input:
    file report from fastqc_report2.collect()

  output:
    file "*multiqc_*" into multiqc_report2

  script:
"""
multiqc -f .
"""
}

process index_fasta {
  tag "$fasta.baseName"
  /* publishDir "results/${params.str}/mapping/index/", mode: 'copy' */
  label "index_fasta"

  input:
    file fasta from fasta_file

  output:
    file "*.index*" into index_files
    file "*_report.txt" into indexing_report

  script:
"""
bowtie2-build --threads ${task.cpus} ${fasta} ${fasta.baseName}.index &> ${fasta.baseName}_bowtie2_report.txt

if grep -q "Error" ${fasta.baseName}_bowtie2_report.txt; then
  exit 1
fi
"""
}

process mapping_fastq {
  tag "$pair_id"
  /* publishDir "results/${params.str}/mapping/bams/", mode: 'copy' */
  label "mapping_fastq"

  input:
    set mutant_id, couverture_level, pair_id, file(reads) from fastq_files2_trim
    file index from index_files.collect()

  output:
    set mutant_id, couverture_level, pair_id, "*.bam" into bam_files, bam_files2
    file "*_report.txt" into mapping_report

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.bt2/ && !(index_file =~ /.*\.rev\.1\.bt2/)) {
        index_id = ( index_file =~ /(.*)\.1\.bt2/)[0][1]
    }
  }

  if( params.str == 'EMS' ){
  """
bowtie2 -I 300 -X 1500 --very-sensitive -p ${task.cpus} -x ${index_id} \
-1 ${reads[0]} -2 ${reads[1]} 2> \
${pair_id}_bowtie2_report_tmp.txt | \
samtools view -Sb - > ${pair_id}.bam

if grep -q "Error" ${pair_id}_bowtie2_report_tmp.txt; then
  exit 1
fi
tail -n 19 ${pair_id}_bowtie2_report_tmp.txt > ${pair_id}_bowtie2_report.txt
  """
  }else if ( params.str == 'BSA' ){ 
      if (couverture_level == 'low'){
      """
      bowtie2 -I 300 -X 1500 --very-sensitive -p ${task.cpus} -x ${index_id} --rg-id ${pair_id} --rg SM:L \
      -1 ${reads[0]} -2 ${reads[1]} 2> \
      ${pair_id}_bowtie2_report_tmp.txt | \
      samtools view -Sb - > ${pair_id}.bam

      if grep -q "Error" ${pair_id}_bowtie2_report_tmp.txt; then
        exit 1
      fi
      tail -n 19 ${pair_id}_bowtie2_report_tmp.txt > ${pair_id}_bowtie2_report.txt
      """
      } else if (couverture_level == "middle") {
      """
      bowtie2 -I 300 -X 1500 --very-sensitive -p ${task.cpus} -x ${index_id} --rg-id ${pair_id} --rg SM:M \
      -1 ${reads[0]} -2 ${reads[1]} 2> \
      ${pair_id}_bowtie2_report_tmp.txt | \
      samtools view -Sb - > ${pair_id}.bam

      if grep -q "Error" ${pair_id}_bowtie2_report_tmp.txt; then
        exit 1
      fi
      tail -n 19 ${pair_id}_bowtie2_report_tmp.txt > ${pair_id}_bowtie2_report.txt
      """
      }else{
      """
      bowtie2 -I 300 -X 1500 --very-sensitive -p ${task.cpus} -x ${index_id} --rg-id ${pair_id} --rg SM:H \
      -1 ${reads[0]} -2 ${reads[1]} 2> \
      ${pair_id}_bowtie2_report_tmp.txt | \
      samtools view -Sb - > ${pair_id}.bam

      if grep -q "Error" ${pair_id}_bowtie2_report_tmp.txt; then
        exit 1
      fi
      tail -n 19 ${pair_id}_bowtie2_report_tmp.txt > ${pair_id}_bowtie2_report.txt
      """
      }
  }else{
    error "Invalid option --str"
  }
}
/*
bam_files2.view() */

/* read_tag allows us to check if the RG tag was successfully added in the bam files
process read_tag {
  tag "$mutant_id"
  publishDir "results/BSA/mapping/bams/tag", mode: 'copy'
  label "sort_bam"

  input:
    set mutant_id, couverture_level, pair_id, file(bam) from bam_files2

  output:
    file "*.txt" into tags

  script:
"""
samtools view -H ${bam} > ${pair_id}_${couverture_level}.txt
"""

}
 */

process multiqc_mapping {
  tag "$report[0].baseName"
  publishDir "results/${params.str}/multiqc_mapping", mode: 'copy'
  label "multiqc"

  input:
    file report from mapping_report.collect()

  output:
    file "*multiqc_*" into multiqc_report_mapp

  script:
"""
multiqc -f .
"""
}

process sort_bam {
  tag "$file_id"
  /* publishDir "results/${params.str}/mapping/bams/sort_bam", mode: 'copy' */
  label "sort_bam"

  input:
    set mutant_id, couverture_level, file_id, file(bam) from bam_files

  output:
    set mutant_id, couverture_level, file_id, "*_sorted.bam" into sorted_bam_files, sorted_bam_files2

  script:
"""
samtools sort -@ ${task.cpus} -O BAM -o ${file_id}_sorted.bam ${bam}
"""
}
/*
sorted_bam_files2.view() */

process clip_overlap {
  tag "$file_id"
  /* publishDir "results/${params.str}/removed_overlap", mode: 'copy' */
  label "clip_overlap_bam"

  input:
    set mutant_id, couverture_level, file_id, file(bam) from sorted_bam_files

  output:
    set mutant_id, couverture_level, file_id, "*.bam*" into clip_overlap_bam_file2, clip_overlap_bam_file3, clip_overlap_bams

  script:
"""
bam clipOverlap --in ${bam} --out ${file_id}_clip_overlap.bam 
"""
}

/* clip_overlap_bams est de ce type :
[mutant_id, couverture_level, pair_id, bam_file]
et bams : 
[mutant_id, [couverture_level, pair_id, bam_file],[couverture_level, pair_id, bam_file],[couverture_level, pair_id, bam_file]] */

clip_overlap_bams
  .map { it ->  [ it[0], [ it[1],it[2],it[3] ] ]}
  .groupTuple(by: 0)
  .set {bams0}
  
bams0
  .map { it ->  [ it[0], it[1][0], it[1][1], it[1][2] ]}
  .view()
  .set {bams}


process snp {
  tag "$file_id"
  publishDir "results/${params.str}/vcf", mode: 'copy'
  label "snp_freebayes"
    
  input:
      set mutant_id, bam1, bam2, bam3 from bams
      file fasta from fasta_file2.collect()

  output:
    set mutant_id, "*.vcf*" into vcf_file

  script:
  if( params.str == 'EMS' )
  
  """
  freebayes --use-best-n-alleles 2 --min-alternate-count 4 --min-alternate-fraction 0.8 -f ${fasta} ${bam1[2]} > ${mutant_id}.vcf
  
  """
  else if ( params.str == 'BSA' ) /* freebayes cherche les snp dans tous les bulks et indique le nombre de reads par snp */
    """
  freebayes --use-best-n-alleles 2 --min-alternate-count 4 --min-alternate-fraction 0.1 --pooled-discrete --pooled-continuous -f ${fasta} ${bam1[2]} ${bam2[2]} ${bam3[2]} > ${mutant_id}.vcf
  """
  else
    error "Invalid option --str"
}

process coverage {
  tag "$pair_id"
  publishDir "results/${params.str}/coverage", mode: 'copy'
  label "bedtools"

  input:
    set mutant_id, couverture_level, pair_id, file(bam) from clip_overlap_bam_file2

  output:
    file "*_coverage.txt" into coverage_file

  script:
"""
bedtools genomecov -d -ibam ${bam} > ${pair_id}_coverage.txt
"""
}


process insert_size {
  tag "$pair_id"
  publishDir "results/${params.str}/insert_size", mode: 'copy'
  label "sort_bam"

  input:
    set mutant_id, couverture_level, pair_id, file(bam) from clip_overlap_bam_file3

  output:
    file "*_insert_size.txt" into insert_size_file

  script:
"""
samtools view -f66 ${bam} | cut -f 9 | sed "s/^-//" > ${pair_id}_insert_size.txt
"""
}


