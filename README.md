# Identification of mutations impacting the expression variability using BSA-Seq

## Short project presentation

### EMS Mutants data
We have 9 sequences of Saccharomyces cerevisiae which where mutated using EMS. The aim is to identify all the mutations, their positions in the genome and their occurance in the 9 sequences.
We assume that mutations which are present in all 9 EMS mutants are from the strain used in the lab which differs a little from the known sequenced reference genome. These mutations are removed from our analysis. 

### BSA-Seq data (segregants resulting from cross between EMS mutant and wild strain)
Each EMS mutant was crossed with the wild type strain and meiosis was induced in the resulting diploid. Each segregant has grown and been distributed in one of three bulks (low, middle or high fluorescence level) using flow cytometry. For each mutant analysed by BSA, each bulk was sequenced. The aim is to analyse these 18 sequences and identify all the mutations, their positions and their frequencies in each bulk. Mutations appearing with the same frequence in each of the three bulks are not interesting because not responsible of the fluorescence variability.

### Determine the mutation responsible for fluorescence variability
To identify the mutation responsible for fluorescence variability, 3 EMS mutants with a lower variability compared to the wild strain variability have been chosen. The mutation sought will most likely be in the central bulk of the mutants analyzed by BSA-Seq, with a different frequency from the extreme bulks (low or high). 
3 EMS mutants with a higher variability compared to the wild strain variability have also been chosen. The mutation sought will most likely be in the extreme bulks with a different frequency from the central bulk.

## Getting Started

### Prerequisities

To run nextflow on you computer you need to have java (>= 1.8) installed.

```sh
java --version
```

To be able to easily test tools already implemented for nextflow on your computer (`src/nf_modules/` to see their list). You need to have docker installed.

```sh
docker run hello-world
```

### Installing

Clone the repository with the following command :
```sh
git clone git@gitbio.ens-lyon.fr:LBMC/gylab/MappingNoise.git
```
Enter the MappingNoise repository with `cd MappingNoise`.

To install nextflow on your computer simply run the following command:

```sh
src/install_nextflow.sh
```

### Running the pipeline
This pipeline can process two types of datasets : EMS or BSA. 

To run the pipeline on the EMS dataset, run :
```sh
./nextflow src/MutantSeq.nf -c src/MutantSeq.config -profile docker --fasta "data/tiny_dataset/ref_genome/*.fsa" --str 'EMS' --inputFromPATH "data/EMS_table_psmn.csv" -resume
```

To run the pipeline on the BSA dataset, run :
```sh
./nextflow src/MutantSeq.nf -c src/MutantSeq.config -profile docker --fasta "data/tiny_dataset/ref_genome/*.fsa" --str 'BSA' --inputFromPATH "data/BSA_table_psmn.csv" -resume
```
#### Pipeline arguments

**-c** : use this to specify the path of the configuration settings file used for this pipeline.

**-profile** : use this to choose a configuration profile. It can be `docker` or `singularity` if they are installed on your machine. Use the profile `psmn` to run the pipeline on lbmc cluster.

**--fasta** : use this to specify the path of the genome fasta file, used as reference for mapping.

**--str** : use this to specify if you want to run the pipeline on `EMS` or `BSA` data.

**--inputFromPATH** : use this to enter the path of csv file
```sh

The CSV file contains the following informations : 

|        mutant_id        |    segregant_population    |                   files                      |
|:-----------------------:|:--------------------------:|:--------------------------------------------:|
|         2P05A05         |            low             |  data/tiny_dataset/BSA/file_id_R{1,2}.fastq  
                         
                             You can specify as many lines as you want

For BSA analysis, you have to specify the segregant population `low`, `middle` or `high` corresponding to the bulk. For EMS analysis, leave this column empty.
Note : Columns must be separated by tabs and columns names must be mutant_id, segregant_population and files.
Examples of csv files are in the /doc repository.
```
**-resume** : use this to used cached results. Nextflow saved results for each process successfully completed. With this parameter, for any process that is unchanged and run for a second time, Nextflow used the cached results.

## Pipeline

### Quality analysis
For the two datasets, we first remove the adaptors with `cutadapt`. A MultiQC analysis is available before and after cut to check that the adaptors where correctly removed.
We then trimm the sequences per quality with `URQt`. A MultiQC analysis is also available at this stage.

### Genome indexing
The reference genome is indexed with `bowtie2`. 

### Genome mapping
The reads previously trimmed are mapped against the indexed reference genome with `bowtie2`. A MultiQC analysis is available at his stage. 
The resulting bam files are sorted with `samtools`.
To remove overlaps, we use `bam clipOverlap` on the sorted bam files.

### SNP
To detect variants, we use `freebayes` and selected some options depending on the dataset. VCF files are available at this stage.

### Coverage
Coverage files are created with `bedtools`.

### Insert size
Insert size files are created with `samtools`.

## VCF treatment

### Step 1 : VCF corresponding to the EMS data
When the pipeline is run on the EMS dataset, it produces one vcf file per EMS mutant. Each vcf file contains all mutations identified by freebayes. 

The python script called `filter_vcf.py` filters all the mutations in the vcf input file. For example, we are only interessed in SNP mutations, mutations with a quality score higher than 200 and having a sequencing depth higher than 5. The script outputs a vcf file containing only the mutations which passed all the filters. For each input file, it displays the number of variants before and after filtering. 

The python script called `filter_occurances.py` counts how many times a mutation appears in all vcf input files. This number is called occurance. We only want the mutations occuring once. This script outputs for each EMS vcf input file, a vcf file containing the mutations with an occurance equal to 1. This script displays for each output file, the number of variants and the proportion (in %) of bases C mutated in T and G in A.

You can modify the bash script `treatment_EMS_vcf.sh` which runs the 2 python scripts and put your input files names and ouptput files names. Then, you only have to run the following command line to run the 2 python scripts :
```sh
./treatment_EMS_vcf.sh > results.txt
```

The file `results.txt` contains all displays from the 2 python scripts.
Note that `treatment_EMS_vcf.sh` must be run in the repository containing all input files.

### Step 2 : VCF corresponding to the BSA data
When the pipeline is run on the BSA dataset, it produces one vcf file per mutant analyzed by BSA. Each of these vcf file contains all mutations present in the 3 bulks and listed by freebayes. For each mutation and for each bulk, the number of reads matching with the wild type allele (RO) and the mutant allele (AO) is available.

We want to make sure that the mutations present in the EMS mutants are also in the BSA sequences. The mutations common to EMS mutants and mutants analyzed by BSA-Seq are listed in a text file (<common_mutations_EMS_BSA>.txt). This file contains for each mutation many informations (DP, RO, AO ..). This file is used to create another text file (<BSA_freq_alleles>.txt) containing for each mutation the RO and the AO for each bulk. This file is used to do some statistical tests in R.

These two text files are generated by a python script called `compare_EMS_BSA.py`. To run this file, open a terminal and place yourself in the following directory `/MappingNoise/src`. Run the following command :

```sh
python compare_EMS_BSA.py <EMS_INPUT_FILE>.vcf <BSA_INPUT_FILE>.vcf <common_mutations_EMS_BSA>.txt <BSA_freq_alleles>.txt
```
Note that the path to the vcf files is not specified in this command line so don't forget to add it if your vcf files are not in the working repository.

## Statistical tests

The R script `tests_freq_alleles.R` computes pvalues and pscores for mutants analyzed by BSA-Seq. 
We computed a first likelihood ratio test with the following table of contingency :
```sh
    Table of contingence for the 
 likelihood ratio test for 3 bulks :

|      |   Low   |  Middle |   High  |
|:----:|:-------:|:-------:|:-------:|
|  RO  |    .    |    .    |    .    |
|  AO  |    .    |    .    |    .    |
```
This test computes the likelihood that the frequencies observed in each bulk (low, middle, high) follow the same distribution and that row and columns are independent. 

We computed another likelihood ratio test with the following table of contingency :

```sh
      Table of contingence for the 
 likelihood ratio test for 2 bulks :

|      |   Low+High  |  Middle |
|:----:|:-----------:|:-------:|
|  RO  |      .      |    .    |   
|  AO  |      .      |    .    |   
```
In this table, the counts of RO and AO of the extreme bulks (low and high) are summed. This test computes the likelihood that the frequencies observed in each bulk (low+high, middle) follow the same distribution and that row and columns are independent. 

The pvalue of this tests are adjusted with the Benjamini-Hochberg (BH) correction. The significance level is fixed at 0.01. To better visualize the results, we ploted the pscore for each mutation and for each test rather than the pvalue. The pscore for the likelihood ratio test for 3 bulks is calculated with the formula -log10(pvalue3bulks) and the pscore for the likelihood ratio test for 2 bulks is calculated with -log10(pvalue2bulks)*sign. Sign is positive if the frequency of AO in the extreme bulk (low+high) is higher than the frequency of AO in the middle bulk, else it is negative.

The R script `plot_coverage.R` plots graphs of the coverage level for the 3 bulks (of one mutant analyzed by BSA-Seq) for each chromosome using 1kb overlapping sliding windows.

## Available tools

[The list of available tools.](doc/available_tools.md)

## Projects using nextflow

[A list of projects using nextflow at the LBMC.](doc/nf_projects.md)

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitbio.ens-lyon.fr/pipelines/nextflow/tags).

## Authors

* **Laurent Modolo** - *Initial work*

See also the list of [contributors](https://gitbio.ens-lyon.fr/pipelines/nextflow/graphs/master) who participated in this project.

## License

This project is licensed under the CeCiLL License- see the [LICENSE](LICENSE) file for details
